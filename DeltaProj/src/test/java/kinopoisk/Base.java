

package kinopoisk;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class Base {

    WebDriver driver = null;
    final String url = "https://hd.kinopoisk.ru/";

    @BeforeMethod
    public void initData() {
        System.setProperty("webdriver.chrome.driver", "Y:\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get(url);
        driver.manage().window().maximize();
    }

    @AfterMethod
    public void stop() {
        driver.quit();
    }

}


