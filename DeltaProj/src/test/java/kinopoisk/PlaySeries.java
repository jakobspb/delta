package kinopoisk;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;
//VERY IMPORTANT!!!
//working with only users, having kinopoisk-HD subscribe, and ever seen chosen series (Doctor House) with their current account
public class  PlaySeries extends Base{
    @Test(description = "Full positive test with logging in and playing the series", priority = 2)
    public void TestPlaySeries()  {

        //startpage to login page
        WebElement LoginButton = driver.findElement(By.xpath("//a[@data-tid-prop='6701e198']"));
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        LoginButton.click();

        //email login page + button to password page
        WebElement UsernameInput = driver.findElement(By.id("passp-field-login"));
        UsernameInput.clear();
        UsernameInput.sendKeys(InputData.Username);
        //email button
        WebElement EmailButton = driver.findElement(By.id("passp:sign-in"));
        EmailButton.click();

        //password login page + button to log in
        WebElement PasswordInput = driver.findElement(By.id("passp-field-passwd"));
        PasswordInput.clear();
        PasswordInput.sendKeys(InputData.Password);

        // login button
        WebElement PasswordButton = driver.findElement(By.id("passp:sign-in"));
        PasswordButton.click();

        //search button click
        WebElement SearchButton = driver.findElement(By.xpath("//button[@data-tid='35c70afc ae3b93d3']"));
        SearchButton.click();

        //search input
        WebElement SearchInput = driver.findElement(By.xpath("//input[@placeholder='Фильмы и сериалы']"));
        SearchInput.clear();
        SearchInput.sendKeys("Доктор Хаус");

        //result clicking
        WebElement SearchResult = driver.findElement(By.xpath("//img[@src='//avatars.mds.yandex.net/get-ott/223007/2a0000017984d2b4f42ef887bee62149f47a/170x255']"));
        SearchResult.click();

        //play button
        WebElement PlayButton = driver.findElement(By.xpath("//div[@data-tid='b669aa9c 9881f1d5 ae3b93d3 76420b0']"));
        PlayButton.click();


        int PauseButton = driver.findElements(By.xpath("//div[@class='Icon__root--2j3kX Icon__root_type_pause--1meWg Icon__root_size_control--2W3ri TransitionIcon__icon--2IP9k TransitionIcon__icon_absolute--2Ye5Z']")).size();
        Assert.assertTrue(PauseButton > 0);

    }


}




