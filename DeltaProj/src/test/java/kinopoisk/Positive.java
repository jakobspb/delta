package kinopoisk;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.Test;


public class Positive extends Base{
    @Test(description = "Positive logging in test", priority = 1 )
    public void TestCorrectDataLogin()  {

        //startpage to login page
        WebElement LoginButton = driver.findElement(By.xpath("//a[@data-tid-prop='6701e198']"));
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        LoginButton.click();

        //email login page + button to password page
        WebElement UsernameInput = driver.findElement(By.id("passp-field-login"));
        UsernameInput.clear();
        UsernameInput.sendKeys(InputData.Username);
        //email button
        WebElement EmailButton = driver.findElement(By.id("passp:sign-in"));
        EmailButton.click();

        //password login page + button to log in
        WebElement PasswordInput = driver.findElement(By.id("passp-field-passwd"));
        PasswordInput.clear();
        PasswordInput.sendKeys(InputData.Password);

        // login button
        WebElement PasswordButton = driver.findElement(By.id("passp:sign-in"));
        PasswordButton.click();


        int Avatar = driver.findElements(By.xpath("//div[@class='ProfileMenu__main-avatar--2eAIB ProfileMenu__dropdown--27_sU']")).size();
        Assert.assertTrue(Avatar > 0);

    }



}
